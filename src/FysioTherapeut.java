import java.time.LocalDate;

public class FysioTherapeut extends Person{
    public FysioTherapeut(int id, String surName, String firstName, LocalDate dateOfBirth) {
        super(id, surName, "fysiotherapeut", dateOfBirth);
    }

    @Override
    void editMenu() {

    }
}
