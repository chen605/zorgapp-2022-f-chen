import java.time.LocalDate;

public abstract class Person {
    private String surName;
    private String firstName;

    private LocalDate dateOfBirth;
    private final int id;

    public Person(int id, String surName, String firstName, LocalDate dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getSurName() {
        return surName;
    }

    public String getFirstName() {
        return firstName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public int getId() {
        return id;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

     abstract void editMenu();

}
