import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;

public class Patient extends Person{
   private final int RETURN = 0;
   private final int SURNAME = 1;
   private final int FIRSTNAME = 2;
   private final int NICKNAME = 3;
   private final int DATEOFBIRTH = 4;
   private final int LENGTH = 5;
   private final int WEIGHT = 6;
   private final int GETPATIENTWEIGHTS = 7;
   private final int DOSAGE = 8;
   private final int GETPATIENTMEDICINES = 9;
   private final int DELETEMEDICINE = 10;
   private final int ADDMEDICINE = 11;
   private final int ADDPATIENTWEIGHT = 12;
   private final int DELETEPATIENTWEIGHT = 13;

   private String nickName;

   private double weight = 0.0;
   private double length = 0.0;
//   private int id = -1;
   private final ArrayList<Medicine> medicines = new ArrayList<>();
   private final ArrayList<Weight> weights = new ArrayList<>();

   // Constructor
   Patient(int id, String surName, String firstName, LocalDate dateOfBirth) {
      super(id, surName, firstName, dateOfBirth);
      this.nickName = firstName;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   Patient(int id, String surName, String firstName, LocalDate dateOfBirth, double length, double weight) {
      this(id, surName, firstName, dateOfBirth);
      this.setLength(length);
      this.setWeight(weight);
   }

//   public JSONObject getJSONObject() {
//      JSONObject obj = new JSONObject();
//      try {
//         obj.put("id", id);
//         obj.put("surName", surName);
//         obj.put("firstName", firstName);
//         obj.put("dateOfBirth", dateOfBirth);
//         obj.put("length", length);
//         obj.put("weight", weight);
//      } catch (JSONException e) {
//         System.out.println("DefaultListItem.toString JSONException: "+e.getMessage());
//      }
//      return obj;
//   }


   public void addMedicine(Medicine medicine){
      medicines.add(medicine);
   }

   public void addWeight(Weight weight){
      weights.add(weight);
   }


//   String pathName = "Patients.json";
//   String contents;

//   {
//      try {
//         contents = new String((Files.readAllBytes(Paths.get(pathName))));
//
//         JSONArray jsonArray = new JSONArray(contents);
////         JSONObject patientObject = (JSONObject) object.get("Patients");
////         System.out.println(jsonArray);
//
//      } catch (IOException e) {
//         e.printStackTrace();
//      }
//   }


   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public double getLength() {
      return length;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void setLength(double length) {
      if(length < 0 ){
         System.out.println("The number you have entered is invalid. Number was negative.");
      }
      else if(length == 0){
         System.out.println("The number you have entered is 0.0. 0.0m is an invalid length. Please enter a number higher then 0.0");
      }
      this.length = length;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public double getWeight() {
      return weight;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void setWeight(double w) {
      if(w < 0 ){
         System.out.println("The number you have entered is invalid. Number was negative.");
      }
      else if(w == 0){
         System.out.println("The number you have entered is 0.0. 0.0kg is weightless. Please enter a number higher then 0.0");
      }
      weight = w;
   }

   // Access callName
   public String getNickName() {
      return nickName;
   }

   public void setNickName(String nickName) {
      this.nickName = nickName;
   }


   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public double calcBMI() {
      return this.weight / Math.pow(length, 2);
   }

   public double calcBmiPoints(double bmi){
      return  bmi / Math.pow(length, 2);
   }

   private int calcAge(LocalDate dateOfBirth) {
      LocalDate currentDate = LocalDate.now();

      if (dateOfBirth != null) {
         return Period.between(dateOfBirth, currentDate).getYears();
      } else {
         return -1;
      }
   }

   // formats String dateOfBirth into LocalDate type
   private LocalDate formatDateOfBirth(String birthDate) {
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      LocalDate formattedDate = LocalDate.parse(birthDate, formatter);
      return formattedDate;
   }

   // Handle editing of patient data (patient menu)
   @Override
   void editMenu() {
      var scanner1 = new BScanner(); // use for numbers.
      var scanner2 = new BScanner(); // use for strings


      boolean nextCycle = true;
      while (nextCycle) {
         System.out.println("Patient data edit menu:");
         printEditMenuOptions(false, null);
         System.out.println("Enter digit (0=return)");

         int choice = scanner1.scanInt();
         switch (choice) {
            case RETURN:
               nextCycle = false;
               break;

            case NICKNAME:
               System.out.format("Enter new nickname: (was: %s)\n", nickName);
               // YOUR CODE  HERE
               String patientNickName = scanner2.scanString();
               this.setNickName(patientNickName);
               break;

            default:
               System.out.println("Invalid entry: " + choice);
               break;
         }
      }
   }

   // Method Overload to edit a patient as a caretaker (caretaker menu)
   // TODO find a way to return to the administration menu as a zv...
   void editMenu(boolean zv, Patient chosenPatient) {
      var scanner1 = new BScanner(); // use for numbers.
      var scanner2 = new BScanner(); // use for strings

//      JSONArray jsonArray = new JSONArray();
//      jsonArray.put(chosenPatient);
////      System.out.println(jsonArray);

//      try(FileWriter fileWriter = new FileWriter("Patients.json")){
//         fileWriter.write(chosenPatient.toString());
//         fileWriter.flush();
//         fileWriter.close();
//         System.out.println("JSON Object sucessfully written to the file!!");
//
//
//      } catch (IOException e) {
//         e.printStackTrace();
//      }


      boolean nextCycle = true;
      if(zv){
         while (nextCycle) {
            System.out.println("Patient data edit menu:");
            printEditMenuOptions(true, chosenPatient);
            System.out.println("Enter digit (0=return)");

            int choice = scanner1.scanInt();
            switch (choice) {
               case RETURN:
                  nextCycle = false;
                  break;

               case SURNAME:
                  System.out.format("Enter new surname of patient: (was: %s)\n", getSurName());
                  // YOUR CODE  HERE
                  String editPatientSurName = scanner2.scanString();
                  chosenPatient.setSurName(editPatientSurName);
//                  JSONObject person = jsonArray.getJSONObject(0);
//                  person.put("surName", editPatientSurName);

                  break;

               case FIRSTNAME:
                  System.out.format("Enter new first name of patient: (was: %s)\n", getFirstName());
                  // YOUR CODE  HERE
                  String patientFirstName = scanner2.scanString();
                  chosenPatient.setFirstName(patientFirstName);
                  break;

               case NICKNAME:
                  System.out.format("Enter new nickname of patient: (was: %s)\n", nickName);
                  // YOUR CODE  HERE
               case DATEOFBIRTH:
                  System.out.format("Enter new date of birth (yyyy-MM-dd; was: %s) of patient\n", getDateOfBirth());
                  // YOUR CODE  HERE
                  String BirthDate = scanner2.scanString();
                  chosenPatient.setDateOfBirth(formatDateOfBirth(BirthDate));
                  break;

               case LENGTH:
                  System.out.format("Enter new length (in m; was: %.2f) of patient", length);
                  // YOUR CODE  HERE;
                  double patientLength = scanner1.scanDouble();
                  chosenPatient.setLength(patientLength);
                  break;

               case WEIGHT:
                  System.out.format("Enter new weight (in kg; was: %.1f) of patient\n", getWeight());
                  // YOUR CODE  HERE
                  double patientWeight = scanner1.scanDouble();
                  chosenPatient.setWeight(patientWeight);
                  break;

               case DOSAGE:
                  for (int i = 0; i< chosenPatient.medicines.size(); i++) {
                     System.out.println(i + ". " + chosenPatient.medicines.get(i).getMedicineName());
                  }
                  System.out.println("Choose a medicine:");
                  int medicineChoice = scanner1.scanInt();
                  System.out.format("Enter new dosage (in mg; was: %s) of medicine per dag\n", chosenPatient.medicines.get(medicineChoice).getDosage());
                  String medicineDosage = scanner2.scanString();
                  chosenPatient.medicines.get(medicineChoice).setDosage(medicineDosage);
                  break;

               case GETPATIENTMEDICINES:
                  System.out.println( "=================MEDICINES==================" );
                  for (Medicine medicine: chosenPatient.medicines) {
                     System.out.format("Medicine name: %s\n",medicine.getMedicineName());
                     System.out.format("Medicine type: %s\n",medicine.getMedicineType());
                     System.out.format("Medicine dosage: %s mg\n",medicine.getDosage());
                     System.out.println( "============================================" );
                  }
                  System.out.println();
                  break;

               case GETPATIENTWEIGHTS:
                  System.out.println(chosenPatient.getFirstName() + " " + chosenPatient.getSurName() + ": ");
                  System.out.println("==========WEIGHT============");
                  chosenPatient.weights.sort(Comparator.comparing(Weight::getDateOfWeight));
                  for(int i = 0; i < chosenPatient.weights.size(); i++){
                     System.out.println( "[" + chosenPatient.weights.get(i).getDateOfWeight() + "] " +
                                          "*".repeat((int)chosenPatient.weights.get(i).getWeight()) +
                                          " (" + chosenPatient.weights.get(i).getWeight() + " kg)"
                                       );
                  }

                  System.out.println("==========BMI============");
                  for(int i = 0; i < chosenPatient.weights.size(); i++){
                     System.out.format("[%s] %s (bmi= %.1f)\n", chosenPatient.weights.get(i).getDateOfWeight(),
                                                              "*".repeat((int) calcBmiPoints(chosenPatient.weights.get(i).getWeight())),
                                                              calcBmiPoints(chosenPatient.weights.get(i).getWeight()));
                  }
                  break;

               case ADDMEDICINE:
                  System.out.println("Medicine name:");
                  String medicineName = scanner2.scanString();
                  System.out.println("Medicine type:");
                  String medicineType = scanner2.scanString();
                  System.out.println("Medicine dosage:");
                  String dosage = scanner2.scanString();

                  chosenPatient.medicines.add(new Medicine(medicineName, medicineType, dosage));
                  break;

               case DELETEMEDICINE:
                  if(chosenPatient.medicines.isEmpty()){
                     System.out.println("There are no medicines prescribed for this patient.");
                  }
                  else{
                     for (int i = 0; i< chosenPatient.medicines.size(); i++) {
                        System.out.println(i + ". " + chosenPatient.medicines.get(i).getMedicineName());
                     }
                     System.out.println("Choose a medicine to DELETE:");
                     int deleteMedicine = scanner1.scanInt();
                     chosenPatient.medicines.remove(deleteMedicine);
                  }
                  break;

               case ADDPATIENTWEIGHT:
                  System.out.println("Enter the date (yyyy-MM-dd):");
                  String dateOfWeight = scanner2.scanString();
                  LocalDate calcDate =  formatDateOfBirth(dateOfWeight);
                  System.out.println("Enter your current weight:");
                  double patientCurrentWeight = scanner1.scanDouble();
                  chosenPatient.weights.add(new Weight(calcDate, patientCurrentWeight));
                  break;

               case DELETEPATIENTWEIGHT:
                  if(chosenPatient.weights.isEmpty()){
                     System.out.println("There are no weights registered for this patient.");
                  }
                  else{
                     for (int i = 0; i< chosenPatient.weights.size(); i++) {
                        System.out.println(i + ". [" + chosenPatient.weights.get(i).getDateOfWeight() + "] "
                                                + chosenPatient.weights.get(i).getWeight()
                        );
                     }
                     System.out.println("Choose a weight to DELETE:");
                     int deleteWeight = scanner1.scanInt();
                     chosenPatient.weights.remove(deleteWeight);
                  }
                  break;
               default:
                  System.out.println("Invalid entry: " + choice);
                  break;
            }
         }
      }
   }



   // Write patient data to screen.
   void printEditMenuOptions(boolean zv, Patient chosenPatient) {
      if(zv){
         System.out.format("%d: %-17s %s\n", SURNAME, "Surname:", getSurName());
         System.out.format("%d: %-17s %s\n", FIRSTNAME, "firstName:", getFirstName());
         System.out.format("%d: %-17s %s\n", NICKNAME, "Nickname:", nickName);
         System.out.format("%d: %-17s %s (age %d)\n", DATEOFBIRTH, "Date of birth:", getDateOfBirth(), calcAge(this.getDateOfBirth()));
         System.out.format("%d: %-17s %.2f\n", LENGTH, "Length:", length);
         System.out.format("%d: %-17s %.2f kg (bmi= %.1f)\n", WEIGHT, "Weight:", getWeight(), calcBMI());
         System.out.format("%d: %-17s\n", GETPATIENTWEIGHTS, "Patient weight & bmi measurements");
         if (!chosenPatient.medicines.isEmpty()){
            System.out.format("%d: %-17s\n", DOSAGE, "Change medicine dosage");
            System.out.format("%d: %-17s\n", GETPATIENTMEDICINES, "Patient medicines");
            System.out.format("%d: %-17s\n", DELETEMEDICINE, "Delete Medicine");
         }
            System.out.format("%d: %-17s\n", ADDMEDICINE, "Add Medicine");
            System.out.format("%d: %-17s\n", ADDPATIENTWEIGHT, "Add patient weight");
            System.out.format("%d: %-17s\n", DELETEPATIENTWEIGHT, "Delete patient weight");

      }
      else {
         System.out.format("%d: %-17s %s\n", NICKNAME, "Nickname:", nickName);
      }
   }


   // Write patient data to screen.  TODO: Combine with printEditMenuOptions
   void write() {
      System.out.println("===================================");
      System.out.format("%-17s %s\n", "Surname:", getSurName());
      System.out.format("%-17s %s\n", "firstName:", getFirstName());
      System.out.format("%-17s %s\n", "Nickname:", nickName);

      // YOUR CODE HER TO CALCULATE AGE
      System.out.format("%-17s %s (age %d)\n", "Date of birth:", getDateOfBirth(), calcAge(this.getDateOfBirth()));
      if(getId() != 0){
         System.out.format("%-17s %.2f\n", "Length:", length);
         System.out.format("%-17s %.2f kg (bmi= %.1f)\n", "Weight:", getWeight(), calcBMI());
      }

      System.out.println("===================================");
   }

   // Write oneline info of patient to screen
   void writeOneliner() {
      if(getId() == 0){
         System.out.format("%d. %-5s %-20s [%s] (%s)", getId(), getFirstName(), getSurName(), getDateOfBirth().toString(),  "Zorgverlener");

      } else{
         System.out.format("%d. %-5s %-20s [%s] (%s) Medicine usage: %s, Weight measurement: %s", getId(), getFirstName(), getSurName(), getDateOfBirth().toString(), "Patient", medicines.isEmpty() ? "NEE" : "JA", weights.isEmpty() ? "NEE" : "JA");
      }
      System.out.println();
   }
}
