import java.util.ArrayList;
import java.util.List;

public class MedicineList {
   List<Medicine> medicineList = new ArrayList<>();

   public MedicineList() {
      medicineList.add(new Medicine("Paracetamol", "2 mg per dag", "Pijnstiller"));
      medicineList.add(new Medicine("Aspirine", "2mg per dag", "Pijnstiller"));
      medicineList.add(new Medicine("Fenprocoumon", "1.5 mg per dag", "Antistollingsmedicijn"));
      medicineList.add(new Medicine("Sertraline", "2.5 mg per dag", "Kalmeringsmiddel"));
   }

   public List<Medicine> getMedicineList() {
      return medicineList;
   }


   // to add medicine to the medicineList when there will be more medicine.
   private void addMedicine(Medicine medicine)
   {
      medicineList.add(medicine);
   }

}
