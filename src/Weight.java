import java.time.LocalDate;

public class Weight {
   private LocalDate dateOfWeight;
   private double weight;
//   private final ArrayList<Weight> patientWeights = new ArrayList<>();

   public Weight(LocalDate dateOfWeight, double weight) {
      this.dateOfWeight = dateOfWeight;
      this.weight = weight;
   }

   public LocalDate getDateOfWeight() {
      return dateOfWeight;
   }

   public double getWeight() {
      return weight;
   }

   //   private void addWeight(Weight weight){
//      patientWeights.add(weight);
//   }

}
