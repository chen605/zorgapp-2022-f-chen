import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Administration
{
   Patient patient;  // The currently selected patient
   Tandarts tandarts;
   FysioTherapeut fysioTherapeut;
   MedicineList medicineList = new MedicineList();
   List<Patient> patients = new ArrayList<>();
   List<Object> employees = new ArrayList<>();
   boolean zv;       // true when 'zorgverlener'; false otherwise

   // Constructor
   Administration( int userID ) {
      // Initialise (list of) patient(s).
      System.out.format("Hard-coded profiles!\n");

      Patient p = new Patient(0, "Verlener", "Zorg", LocalDate.of(2000, 2, 21));
      patients.add(p);

      p = new Patient(1, "Van Puffelen", "Adriaan", LocalDate.of(2000, 5, 17), 1.65, 57);
      p.addMedicine(new Medicine(medicineList.getMedicineList().get(0)));
      p.addMedicine(new Medicine(medicineList.getMedicineList().get(2)));
      p.addWeight(new Weight(LocalDate.of(2011, 1, 17), 60));
      p.addWeight(new Weight(LocalDate.of(2013, 2, 18), 63));
      p.addWeight(new Weight(LocalDate.of(2015, 3, 19), 66));
      patients.add(p);

      p = new Patient(2, "Boeker", "Tim", LocalDate.of(2000, 7, 12), 1.70, 65);
      p.addMedicine(new Medicine(medicineList.getMedicineList().get(2)));
      p.addMedicine(new Medicine(medicineList.getMedicineList().get(3)));
      p.addWeight(new Weight(LocalDate.of(2011, 1, 17), 68));
      p.addWeight(new Weight(LocalDate.of(2013, 2, 18), 71));
      p.addWeight(new Weight(LocalDate.of(2015, 3, 19), 74));
      patients.add(p);

      p = new Patient(3, "Doe", "John", LocalDate.of(2000, 1, 19), 1.63, 63);
      p.addWeight(new Weight(LocalDate.of(2011, 1, 17), 61));
      p.addWeight(new Weight(LocalDate.of(2013, 2, 18), 58));
      p.addWeight(new Weight(LocalDate.of(2015, 3, 19), 55));
      patients.add(p);

      p = new Patient(4, "Doe", "Jane", LocalDate.of(2000, 10, 2), 1.77, 80);
      patients.add(p);
      patients.add(new Patient(5, "The Cat", "Tom", LocalDate.of(2000, 12, 29)));

      zv = (userID == 0);

   }
   // Search for patient by ID.
   private void searchPatient(int patientId) {
      for (Patient patient : this.patients) {
         if(patient.getId() == patientId) {
            this.patient = patient;
            System.out.println("You have successfully logged in");
         }
      }
   }

   public Patient selectPatient(int choice) {
      for (Patient patient : patients) {
         if (choice == patient.getId()) {
            this.patient = patient;
         }
      }
      return patient;
   }

   // Main menu
   void menu(int userID)
   {
      final int STOP  = 0;
      final int PRINT = 2;
      final int EDIT  = 4;
//      final int  = 6;
      final int EDITPATIENT = 6;
      final int GETMEDICINELIST = 8;
      final int GETPATIENTLIST = 10;
      var scanner = new BScanner();

      if(userID == 0){
         for (Patient patient : patients) {
            if (patient.getId() == 0){
               continue;
            }
            patient.writeOneliner();
         }

         System.out.println("Choose a patient to edit:");

         int patientNumber = scanner.scanInt();
//               JSONObject chosenPatient = selectPatient(patientNumber).getJSONObject();
         if(patientNumber >= patients.size()){
            System.out.println("Patient does not exist please enter a valid number.");
            menu(0);
         }
         Patient chosenPatient = selectPatient(patientNumber);
         patient.editMenu(zv, chosenPatient);
      }


      searchPatient(userID);

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.format( "Current user: " );
         patient.writeOneliner();

         ////////////////////////
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  STOP\n", STOP );
         System.out.format( "%d:  Print %s data\n", PRINT, userID == 0 ? "caretaker" : "patient" );
         System.out.format( "%d:  Edit personal data\n", EDIT );
         if (this.patient.getId() == 0){
            System.out.format( "%d:  Edit patient data\n", EDITPATIENT );
            System.out.format( "%d:  Print medicine list\n", GETMEDICINELIST );
         }
         ////////////////////////

         System.out.println( "enter digit:" );
         int choice = scanner.scanInt();

         switch (choice) {
            case STOP -> // interrupt the loop
                    nextCycle = false;
            case PRINT -> patient.write();
            case EDIT -> {
               System.out.println();
               patient.editMenu(); // Could/Should change when multiple types of zv.
            }
            case EDITPATIENT -> {
               for (Patient patient : patients) {
                  if (patient.getId() == 0) {
                     continue;
                  }
                  patient.writeOneliner();
               }
               System.out.println("Choose a patient to edit:");
               int patientId = scanner.scanInt();
//               JSONObject chosenPatient = selectPatient(patientNumber).getJSONObject();
               if (patientId >= patients.size() || patientId == 0) {
                  System.out.println("Patient does not exist please enter a valid number.");
                  menu(0);
               }
               Patient selectedPatient = selectPatient(patientId);
               patient.editMenu(zv, selectedPatient);
               this.selectPatient(0);
            }
            case GETMEDICINELIST -> {
               System.out.println();
               for (Medicine medicine : medicineList.getMedicineList()) {
                  System.out.println("Name: " + medicine.getMedicineName() + ", Dosage: " + medicine.getDosage() + " mg, Type: " + medicine.getMedicineType());
               }
            }
            case GETPATIENTLIST -> {
               System.out.println();
               for (Patient patient : patients) {
                  if (patient.getId() == 0) {
                     continue;
                  }
                  patient.writeOneliner();
               }
            }

            //TODO probable feature.
//            case 12:
//               for (Medicine medicine: medicineList.getMedicineList()) {
//                  System.out.println(medicine.getMedicineName() + ", " + medicine.getDosage() + " mg, " + medicine.getMedicineType());
//               }
//
//               System.out.println("Choose a medicine:");
//               int medicineChoice = scanner.scanInt();
//               System.out.println( "Name: " + medicineList.getMedicineList().get(medicineChoice).getMedicineName() + ", Type: "
//                                 + medicineList.getMedicineList().get(medicineChoice).getMedicineType() + ", Dosage: "
//                                 + medicineList.getMedicineList().get(medicineChoice).getDosage() + " mg"
//                                 );
//
//               System.out.println("Enter new dosage (in mg; was " + medicineList.getMedicineList().get(medicineChoice).getDosage() + " mg)" );
//               Medicine selectedMedicine = selectMedicine(medicineChoice);
//               double dosage = scanner.scanDouble();
//               while (dosage < 0){
//                  System.out.println("Please enter a valid dosage. The dosage cannot be negative or 0.0 mg");
//                  dosage = scanner.scanDouble();
//               }
//               selectedMedicine.setDosage(dosage);
//
//               System.out.println("Successfully changed the dosage of the medicine");
//

            default -> System.out.println("Please enter a *valid* digit");
         }
      }
   }
}
