public class Medicine {
   private final String medicineName;
   private String dosage;
   private final String medicineType;

   public Medicine(String medicineName, String dosage, String medicineType) {
      this.medicineName = medicineName;
      this.dosage = dosage;
      this.medicineType = medicineType;
   }

   public Medicine(Medicine medicine){
      this.medicineName = medicine.medicineName;
      this.dosage = medicine.dosage;
      this.medicineType = medicine.medicineType;
   }



   public String getMedicineName() {
      return medicineName;
   }

   public String getDosage() {
      return dosage;
   }

   public String getMedicineType() {
      return medicineType;
   }

   public void setDosage(String dosage) {
      this.dosage = dosage;
   }
}
